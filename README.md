# compilation_LAGHOUATI_MOUTAOUKIL_SOMMER_ZENNER

![IDMC](/assets/images/idmc.png)
Lien vers le **bitbucket** : [Bitbucket](https://bitbucket.org/julienzenner/compilation_laghouati_moutaoukil_sommer_zenner/)

*******************************************************************************
*******************************************************************************

Ce projet est un compilateur en bêta uasm.

* Langage impératif
* Type de données : entier
* Opérations : +,-,*,/,>,<,>=,<=,==,!=
* Autres opérations : lecture, appel de fonction
* Instructions
	* Affectation
	* Alternative
	* Répétitive
	* Appel d’un sous-programme
	* Ecriture...
	
* Sous-programme : fonction / procédure
	* Avec des paramètres
	* Et des variables locales
	* Possibilité d’appels récursifs
	
* Commentaires