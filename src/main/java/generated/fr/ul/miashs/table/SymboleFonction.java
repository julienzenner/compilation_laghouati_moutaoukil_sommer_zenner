package generated.fr.ul.miashs.table;

/**
 * Classe Symbole définissant les attributs spécifiques à toutes les fonctions
 */
public class SymboleFonction extends Symbole {
	private int nbParametre;
	private int nbBloc;

	/**
	 * Constructeur de la classe {@link SymboleFonction}
	 * @param nom
	 * @param type
	 * @param nbParametre
	 * @param nbBloc
	 */
	public SymboleFonction(String nom, Type type, int nbParametre, int nbBloc) {
		super(nom, type, "global", Genre.FONCTION);
		this.nbParametre = nbParametre;
		this.nbBloc = nbBloc;
	}

	public int getNbParametre() {
		return nbParametre;
	}

	public void setNbParametre(int nbParametre) {
		this.nbParametre = nbParametre;
	}

	public int getNbBloc() {
		return nbBloc;
	}

	public void setNbBloc(int nbBloc) {
		this.nbBloc = nbBloc;
	}

	@Override
	public String toString() {
		return "SymboleFonction [nbParametre=" + nbParametre + ", nbBloc=" + nbBloc + ", nom=" + nom + ", type=" + type
				+ ", scope=" + scope + ", genre=" + genre + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + nbBloc;
		result = prime * result + nbParametre;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SymboleFonction other = (SymboleFonction) obj;
		if (nbBloc != other.nbBloc)
			return false;
		if (nbParametre != other.nbParametre)
			return false;
		return true;
	}
}