package generated.fr.ul.miashs.table;

/**
 * Définit les types possibles des variables, paramètres, fonction dans la
 * {@link TableDesSymboles}
 */
public enum Type {
	INT, VOID;
}
