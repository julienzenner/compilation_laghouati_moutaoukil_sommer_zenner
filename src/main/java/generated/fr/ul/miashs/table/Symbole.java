package generated.fr.ul.miashs.table;

/**
 * Classe <Strong>abstraite</Strong> définissant les attributs commun à tous les
 * symboles
 */
public abstract class Symbole {
	protected String nom;
	protected Type type;
	protected String scope;
	protected Genre genre;

	/**
	 * Constructeur de la classe {@link Symbole}
	 * @param nom
	 * @param type
	 * @param scope
	 * @param genre
	 */
	public Symbole(String nom, Type type, String scope, Genre genre) {
		super();
		this.nom = nom;
		this.type = type;
		this.scope = scope;
		this.genre = genre;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	@Override
	public String toString() {
		return "Symbole [nom=" + nom + ", type=" + type + ", scope=" + scope + ", genre=" + genre + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((genre == null) ? 0 : genre.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((scope == null) ? 0 : scope.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Symbole other = (Symbole) obj;
		if (genre != other.genre)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (scope != other.scope)
			return false;
		if (type != other.type)
			return false;
		return true;
	}
}