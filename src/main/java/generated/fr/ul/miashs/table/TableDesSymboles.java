package generated.fr.ul.miashs.table;

import java.util.ArrayList;

/**
 * Classee regroupant tous les {@link Symbole} d'un programme dans une
 * {@link ArrayList}
 */
public class TableDesSymboles {
	private ArrayList<Symbole> tableDesSymboles;

	/**
	 * Constructeur de la classe {@link TableDesSymboles}
	 */
	public TableDesSymboles() {
		this.tableDesSymboles = new ArrayList<Symbole>();
	}

	public ArrayList<Symbole> getTableDesSymboles() {
		return tableDesSymboles;
	}

	public void setTableDesSymboles(ArrayList<Symbole> tableDesSymboles) {
		this.tableDesSymboles = tableDesSymboles;
	}

	/**
	 * Ajoute le {@link Symbole} seulement s'il n'existe pas déjà dans la table.<br>
	 * Ou s'il n'est pas déjà dans le scope global
	 * 
	 * @param symbole
	 * @throws Exception
	 */
	public void ajouterSymbole(Symbole symbole) throws Exception {
		if (findSymbole(symbole) == null) {
			this.tableDesSymboles.add(symbole);
		} else {
			Symbole sym = findSymbole(symbole);
			if (sym.getScope().equals(symbole.getScope())) {
				throw new Exception("Double définition");
			} else if ((sym.getScope().equals("global") || symbole.getScope().equals("global"))
					&& sym.getGenre().equals(Genre.VARIABLE) && symbole.getGenre().equals(Genre.VARIABLE)) {
				throw new Exception("Variable déja définie");
			} else {
				this.tableDesSymboles.add(symbole);
			}
		}
	}

	/**
	 * Supprime le {@link Symbole} dans l'{@link ArrayList} spécifique
	 * 
	 * @param sym
	 */
	public void supprimerSymbole(Symbole sym) {
		this.tableDesSymboles.remove(sym);
	}

	/**
	 * Méthode permettant de trouver si le {@link Symbole} existe dans
	 * l'{@link ArrayList}
	 * 
	 * @param sym
	 * @return
	 */
	public Symbole findSymbole(Symbole sym) {
		int i = 0;
		for (Symbole symbole : this.tableDesSymboles) {
			if (sym.getNom().equals(symbole.getNom()) && sym.getGenre().equals(symbole.getGenre())
					&& (sym.getScope().equals(symbole.getScope()) || sym.getScope().equals("global")
							|| symbole.getScope().equals("global"))) {
				return this.tableDesSymboles.get(i);
			}
			i++;
		}
		return null;
	}

	/**
	 * Méthode permettant de trouver si un {@link Symbole} existe dans
	 * l'{@link ArrayList} en cherchant via son nom et son scope
	 * 
	 * @param nom
	 * @param scope
	 * @return
	 */
	public Symbole findSymbole(String nom, String scope) {
		int i = 0;
		for (Symbole symbole : this.tableDesSymboles) {
			if (nom.equals(symbole.getNom())
					&& (scope.equals(symbole.getScope()) || symbole.getScope().equals("global"))) {
				return this.tableDesSymboles.get(i);
			}
			i++;
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tableDesSymboles == null) ? 0 : tableDesSymboles.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TableDesSymboles other = (TableDesSymboles) obj;
		if (tableDesSymboles == null) {
			if (other.tableDesSymboles != null)
				return false;
		} else if (!tableDesSymboles.equals(other.tableDesSymboles))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TableDesSymboles [tableDesSymboles=" + tableDesSymboles + "]";
	}
}
