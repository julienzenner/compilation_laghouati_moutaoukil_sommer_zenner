package generated.fr.ul.miashs.table;

/**
 * Différents genre possibles des symboles stockés dans la
 * {@link TableDesSymboles}
 */
public enum Genre {
	VARIABLE, FONCTION, PARAM;
}
