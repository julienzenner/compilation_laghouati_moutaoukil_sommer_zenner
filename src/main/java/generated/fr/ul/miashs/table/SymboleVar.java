package generated.fr.ul.miashs.table;

/**
 * Classe Symbole définissant les attributs spécifiques à toutes les variables
 */
public class SymboleVar extends Symbole {
	private int valeur;

	/**
	 * Constructeur de la classe {@link SymboleVar}
	 * @param nom
	 * @param scope
	 * @param valeur
	 */
	public SymboleVar(String nom, String scope, int valeur) {
		super(nom, Type.INT, scope, Genre.VARIABLE);
		this.valeur = valeur;
	}

	public int getValeur() {
		return valeur;
	}

	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	@Override
	public String toString() {
		return "SymboleVar [valeur=" + valeur + ", nom=" + nom + ", type=" + type + ", scope=" + scope + ", genre="
				+ genre + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + valeur;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SymboleVar other = (SymboleVar) obj;
		if (valeur != other.valeur)
			return false;
		return true;
	}
}