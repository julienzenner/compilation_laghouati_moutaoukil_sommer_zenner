package generated.fr.ul.miashs.table;

/**
 * Classe Symbole définissant les attributs spécifique aux paramètres de fonction
 */
public class SymboleParam extends Symbole {
	int rang;

	/**
	 * Constructeur de la classe {@link SymboleParam}
	 * @param nom
	 * @param scope
	 * @param rang
	 */
	public SymboleParam(String nom, String scope, int rang) {
		super(nom, Type.INT, scope, Genre.PARAM);
		this.rang = rang;
	}

	public int getRang() {
		return rang;
	}

	public void setRang(int rang) {
		this.rang = rang;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + rang;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SymboleParam other = (SymboleParam) obj;
		if (rang != other.rang)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SymboleParam [rang=" + rang + ", nom=" + nom + ", type=" + type + ", scope=" + scope + ", genre="
				+ genre + "]";
	}

}
