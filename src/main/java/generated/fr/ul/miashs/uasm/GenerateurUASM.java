package generated.fr.ul.miashs.uasm;

import fr.ul.miashs.api.arbre.Affectation;
import fr.ul.miashs.api.arbre.Appel;
import fr.ul.miashs.api.arbre.Const;
import fr.ul.miashs.api.arbre.Different;
import fr.ul.miashs.api.arbre.Division;
import fr.ul.miashs.api.arbre.Ecrire;
import fr.ul.miashs.api.arbre.Egal;
import fr.ul.miashs.api.arbre.Fonction;
import fr.ul.miashs.api.arbre.Idf;
import fr.ul.miashs.api.arbre.Inferieur;
import fr.ul.miashs.api.arbre.InferieurEgal;
import fr.ul.miashs.api.arbre.Moins;
import fr.ul.miashs.api.arbre.Multiplication;
import fr.ul.miashs.api.arbre.Noeud;
import fr.ul.miashs.api.arbre.Plus;
import fr.ul.miashs.api.arbre.Retour;
import fr.ul.miashs.api.arbre.Si;
import fr.ul.miashs.api.arbre.Superieur;
import fr.ul.miashs.api.arbre.SuperieurEgal;
import fr.ul.miashs.api.arbre.TantQue;
import generated.fr.ul.miashs.table.Genre;
import generated.fr.ul.miashs.table.Symbole;
import generated.fr.ul.miashs.table.SymboleFonction;
import generated.fr.ul.miashs.table.SymboleVar;
import generated.fr.ul.miashs.table.TableDesSymboles;

/**
 * Classe permettant de générer le code assembleur à partir de :
 * <ul>
 * <li>arbre</li>
 * <li>table des symboles</li>
 * </ul>
 * associée
 */
public class GenerateurUASM {
	private Noeud noeud;
	private TableDesSymboles tds;
	private String resultat;
	private Symbole scope;

	/**
	 * Constructeur de la classe {@link GenerateurUASM}
	 * 
	 * @param noeud
	 * @param tds
	 */
	public GenerateurUASM(Noeud noeud, TableDesSymboles tds) {
		super();
		this.noeud = noeud;
		this.tds = tds;
		this.resultat = "";
		this.scope = null;
	}

	public String getResultat() {
		return resultat;
	}

	public Symbole getScope() {
		return scope;
	}

	public void setScope(Symbole scope) {
		this.scope = scope;
	}

	/**
	 * Méthode publique pour créer le code assembleur
	 * 
	 * @return <Strong>String</Strong> resultat (code UASM)
	 */
	public String genererCode() {
		genererProg(this.noeud);
		return this.resultat;
	}

	private void genererProg(Noeud noeud) {
		this.resultat += ".include beta.uasm\n.include intio.uasm\n.options tty\n\tCMOVE(pile, SP)\n\tBR(debut)";
		genererVariableGlobal();
		for (Noeud n : this.noeud.getFils()) {
			genererFonction(n);
		}
		this.resultat += "\ndebut:\n\tCALL(main)\n\tHALT()";
		this.resultat += "\npile :";
	}

	private void genererVariable(String scope) {
		for (Symbole symbole : tds.getTableDesSymboles()) {
			if (symbole.getScope() == scope && symbole.getGenre() == Genre.VARIABLE) {
				this.resultat += "\n" + symbole.getNom() + ":\tLONG(" + ((SymboleVar) symbole).getValeur() + ")";
			}
		}
	}

	private void genererVariableGlobal() {
		genererVariable("global");
	}

	private void genererVariableLocale(String scope) {
		genererVariable(scope);
	}

	private void genererFonction(Noeud noeud) {
		this.setScope(tds.findSymbole(((Fonction) noeud).getValeur().toString(), "global"));
		this.resultat += "\n" + ((Fonction) noeud).getValeur().toString() + ":";
		genererVariableLocale(((Fonction) noeud).getValeur().toString());
		this.resultat += "\n\tPUSH(LP)\n\tPUSH(BP)\n\tMOVE(SP,BP)\n\tALLOCATE(" + noeud.getFils().size() + ")";
		genererBloc(noeud.getFils().get(0));
		this.resultat += "\nret" + ((Fonction) noeud).getValeur().toString() + ":";
		this.resultat += "\n\tDEALLOCATE(" + noeud.getFils().size() + ")\n\tPOP(BP)\n\tPOP(LP)\n\tRTN()";
	}

	private void genererBloc(Noeud noeud) {
		for (Noeud n : noeud.getFils()) {
			genererInstruction(n);
		}
	}

	private void genererAffectation(Noeud noeud) {
		genererExpression(((Affectation) noeud).getFilsDroit());
		this.resultat += "\n\tPOP(R0)\n\tST(R0," + ((Idf) ((Affectation) noeud).getFilsGauche()).getValeur() + ")";
	}

	private void genererAppel(Noeud noeud) {
		this.resultat += "\n\tALLOCATE(1)";
		if (noeud.getFils().isEmpty()) {
			this.resultat += "\n\tCALL(" + ((Appel) noeud).getValeur() + ")";
		} else {
			for (Noeud n : noeud.getFils()) {
				genererExpression(n);
			}
			this.resultat += "\n\tCALL(" + ((Appel) noeud).getValeur() + "," + noeud.getFils().size() + ")";
		}
		this.resultat += "\n\tDEALLOCATE(" + noeud.getFils().size() + ")";
	}

	private void genererEcrire(Noeud noeud) {
		genererExpression(((Ecrire) noeud).getLeFils());
		this.resultat += "\n\tPOP(R0)\n\tWRINT()";
	}

	private void genererRetour(Noeud noeud) {
		genererExpression(((Retour) noeud).getLeFils());
		this.resultat += "\n\tPOP(R0)";
		this.resultat += "\n\tPUTFRAME(R0," + (1 + ((SymboleFonction) this.scope).getNbParametre()) * -4 + ")";
		this.resultat += "\n\tBR(" + ((Retour) noeud).getValeur() + ")";
	}

	private void genererTantQue(Noeud noeud) {
		this.resultat += "\n\tBR(tq" + ((TantQue) noeud).getValeur() + ")\ntq" + ((TantQue) noeud).getValeur() + ":";
		genererCondition(((TantQue) noeud).getCondition());
		this.resultat += "\n\tPOP(R0)\n\tBF(R0,ftq" + ((TantQue) noeud).getValeur() + ")";
		genererBloc(((TantQue) noeud).getBloc());
		this.resultat += "\n\tBR(tq" + ((TantQue) noeud).getValeur() + ")\nftq" + ((TantQue) noeud).getValeur() + ":";
	}

	private void genererInstruction(Noeud noeud) {
		switch (noeud.getCat()) {
		case AFF:
			genererAffectation(noeud);
			break;
		case APPEL:
			genererAppel(noeud);
			break;
		case ECR:
			genererEcrire(noeud);
			break;
		case RET:
			genererRetour(noeud);
			break;
		case SI:
			genererSi(noeud);
			break;
		case TQ:
			genererTantQue(noeud);
			break;
		default:
			break;
		}
	}

	private void genererEgal(Noeud noeud) {
		genererExpression(((Egal) noeud).getFilsGauche());
		genererExpression(((Egal) noeud).getFilsDroit());
		this.resultat += "POP(R2)\n\tPOP(R1)\n\tCMPEQ(R1, R2, R0)\n\tPUSH(R0)";
	}

	private void genererDifferent(Noeud noeud) {
		genererExpression(((Different) noeud).getFilsGauche());
		genererExpression(((Different) noeud).getFilsDroit());
		this.resultat += "POP(R2)\n\tPOP(R1)\n\tCMPEQ(R1, R2, R0)\n\tXORC(R0,1,R0)\n\tPUSH(R0)";
	}

	private void genererInferieur(Noeud noeud) {
		genererExpression(((Inferieur) noeud).getFilsGauche());
		genererExpression(((Inferieur) noeud).getFilsDroit());
		this.resultat += "POP(R2)\n\tPOP(R1)\n\tCMPLT(R1, R2, R0)\n\tPUSH(R0)";
	}

	private void genererInferieurEgal(Noeud noeud) {
		genererExpression(((InferieurEgal) noeud).getFilsGauche());
		genererExpression(((InferieurEgal) noeud).getFilsDroit());
		this.resultat += "POP(R2)\n\tPOP(R1)\n\tCMPLE(R1, R2, R0)\n\tPUSH(R0)";
	}

	private void genererSuperieur(Noeud noeud) {
		genererExpression(((Superieur) noeud).getFilsGauche());
		genererExpression(((Superieur) noeud).getFilsDroit());
		this.resultat += "POP(R2)\n\tPOP(R1)\n\tCMPLE(R2, R1, R0)\n\tPUSH(R0)";
	}

	private void genererSuperieurEgal(Noeud noeud) {
		genererExpression(((SuperieurEgal) noeud).getFilsGauche());
		genererExpression(((SuperieurEgal) noeud).getFilsDroit());
		this.resultat += "POP(R2)\n\tPOP(R1)\n\tCMPLE(R2, R1, R0)\n\tPUSH(R0)";
	}

	private void genererCondition(Noeud noeud) {
		switch (noeud.getCat()) {
		case EG:
			genererEgal(noeud);
			break;
		case DIF:
			genererDifferent(noeud);
			break;
		case INF:
			genererInferieur(noeud);
			break;
		case INFE:
			genererInferieurEgal(noeud);
			break;
		case SUP:
			genererSuperieur(noeud);
			break;
		case SUPE:
			genererSuperieurEgal(noeud);
			break;
		default:
			break;
		}
	}

	private void genererSi(Noeud noeud) {
		this.resultat += "\nsi" + ((Si) noeud).getValeur() + ":";
		genererCondition(((Si) noeud).getCondition());
		this.resultat += "\n\tPOP(R0)";
		this.resultat += "\n\tBF(R0,sinon" + ((Si) noeud).getValeur() + ")";
		genererBloc(((Si) noeud).getBlocAlors());
		this.resultat += "\n\tBR(finsi" + ((Si) noeud).getValeur() + ")";
		this.resultat += "\nsinon" + ((Si) noeud).getValeur() + ":";
		genererBloc(((Si) noeud).getBlocSinon());
		this.resultat += "\nfinsi" + ((Si) noeud).getValeur() + ":";
	}

	private void genererExpression(Noeud noeud) {
		switch (((Noeud) noeud).getCat()) {
		case CONST:
			genererConst(noeud);
			break;
		case IDF:
			genererIdf(noeud);
			break;
		case PLUS:
			genererPlus(noeud);
			break;
		case MOINS:
			genererMoins(noeud);
			break;
		case MUL:
			genererMul(noeud);
			break;
		case DIV:
			genererDiv(noeud);
			break;
		case LIRE:
			genererLire(noeud);
			break;
		case ECR:
			genererEcrire(noeud);
		case APPEL:
			genererAppel(noeud);
			break;
		default:
			break;
		}
	}

	private void genererDiv(Noeud noeud) {
		genererExpression(((Division) noeud).getFilsGauche());
		genererExpression(((Division) noeud).getFilsDroit());
		this.resultat += "\n\tPOP(R0)\n\tPOP(R1)\n\tDIV(R0,R1,R2)\n\tPUSH(R2)";
	}

	private void genererMul(Noeud noeud) {
		genererExpression(((Multiplication) noeud).getFilsGauche());
		genererExpression(((Multiplication) noeud).getFilsDroit());
		this.resultat += "\n\tPOP(R0)\n\tPOP(R1)\n\tMUL(R0,R1,R2)\n\tPUSH(R2)";
	}

	private void genererMoins(Noeud noeud) {
		genererExpression(((Moins) noeud).getFilsGauche());
		genererExpression(((Moins) noeud).getFilsDroit());
		this.resultat += "\n\tPOP(R0)\n\tPOP(R1)\n\tSUB(R0,R1,R2)\n\tPUSH(R2)";
	}

	private void genererPlus(Noeud noeud) {
		genererExpression(((Plus) noeud).getFilsGauche());
		genererExpression(((Plus) noeud).getFilsDroit());
		this.resultat += "\n\tPOP(R0)\n\tPOP(R1)\n\tADD(R0,R1,R2)\n\tPUSH(R2)";
	}

	private void genererIdf(Noeud noeud) {
		if (this.tds.findSymbole(((Idf) noeud).getValeur().toString(), "global") != null
				&& this.tds.findSymbole(((Idf) noeud).getValeur().toString(), "global").getGenre() != Genre.PARAM) {
			this.resultat += "\n\tLD("
					+ ((SymboleVar) this.tds.findSymbole(((Idf) noeud).getValeur().toString(), "global")).getValeur()
					+ ",R0)\n\tPUSH(R0)";
		} else if (this.tds.findSymbole(((Idf) noeud).getValeur().toString(), scope.getNom()) != null && this.tds
				.findSymbole(((Idf) noeud).getValeur().toString(), scope.getNom()).getGenre() != Genre.PARAM) {
			this.resultat += "\n\tGETFRAME("
					+ ((SymboleVar) this.tds.findSymbole(((Idf) noeud).getValeur().toString(), scope.getNom()))
							.getValeur()
					+ ",R0)\n\tPUSH(R0)";
		}
	}

	private void genererConst(Noeud noeud) {
		this.resultat += "\n\tCMOVE(" + ((Const) noeud).getValeur() + ", R0)";
		this.resultat += "\n\tPUSH(R0)";
	}

	private void genererLire(Noeud noeud) {
		this.resultat += "\n\tPOP(R0)\n\tRDINT()";
	}
}