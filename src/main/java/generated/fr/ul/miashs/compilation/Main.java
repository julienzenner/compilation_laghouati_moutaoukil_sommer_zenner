package generated.fr.ul.miashs.compilation;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Reader;

import fr.ul.miashs.api.arbre.Afficheur;
import generated.fr.ul.miashs.table.Symbole;

/**
 * Classe qui appelle le parser<br>
 * La lecture du flux de carractère se fait sur l'entréee standard
 */
public class Main {

	/**
	 * @param args
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws FileNotFoundException {
		int i = 1; //Changer la valeur de i pour lancer un des tests
		String filenameIn = "samples/test-suite/e" + i + ".data";
		String filenameOut = "samples/uasm/e" + i + ".uasm";
		try {
			Reader reader = new FileReader(filenameIn);
			Yylex scanner = new Yylex(reader);
			@SuppressWarnings("deprecation")
			ParserCup parser = new ParserCup(scanner);
			parser.parse();
			System.out.println(filenameIn + " \n\tTerminé !");
			Afficheur.afficher(parser.resultat);
			for(Symbole s : parser.tds.getTableDesSymboles()) {
				System.out.println(s.toString());
			}
			FileWriter fileWriter = new FileWriter(filenameOut);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.print(parser.uasm.getResultat());
			printWriter.close();
		} catch (Exception e) {
			System.err.println("Erreur !" + e.getMessage());
		}
	}
}