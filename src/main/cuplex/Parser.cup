// Grammaire CUP du projet Compilation
package generated.fr.ul.miashs.compilation;

import fr.ul.miashs.api.arbre.*;
import generated.fr.ul.miashs.table.*;
import generated.fr.ul.miashs.uasm.GenerateurUASM;
import java.util.ArrayList;
/*code java*/
parser code {:
	public Prog resultat = new Prog();
	public TableDesSymboles tds = new TableDesSymboles();
	public GenerateurUASM uasm = new GenerateurUASM(resultat, tds);
	public TableDesSymboles tdsTempVar = new TableDesSymboles();
	public ArrayList<String> usedVar = new ArrayList<String>();
	public ArrayList<String> usedFun = new ArrayList<String>();
	public ArrayList<Integer> usedFunParam = new ArrayList<Integer>();
	public int compteurParamAppel = 0;
	public int compteurSi = 0;
	public int compteurTq = 0;
:}

action code {:
	// Fonction permettant d'ajouter les variables à la table des symboles, vérifie aussi qu'il n'y est pas de double définition
	public void addVarsToTds(String idf, ArrayList<String> vars, String scope, int value) throws Exception {
		tds.ajouterSymbole(new SymboleVar(idf,scope,value));
		if (vars != null) {
			for (String s : vars ) {
				tds.ajouterSymbole(new SymboleVar(s,scope,value));
			}
		}
	}

	//Fonction permettant d'ajouter les variables à une table temporaire pour à la fin set le bon scope.
	public void addVarsToTdsTemp(String idf, ArrayList<String> vars, int value) throws Exception {
		tdsTempVar.getTableDesSymboles().add(new SymboleVar(idf,null,value));
		if (vars != null) {
			for (String s : vars ) {
				tdsTempVar.getTableDesSymboles().add(new SymboleVar(s,null,value));
			}
		}
	}
	
	//Ajoute toute la tds temp à la tds en settant le scope et ajoute aussi les paramètres d'une fonction à la tds
	public void addAllToTds(String idf, Type type, ArrayList<String> params) throws Exception {
		SymboleFonction symFun = new SymboleFonction(idf, type, 0, 0);
		tds.ajouterSymbole(new SymboleFonction(idf, type, 0, 0));
		int i = 0;
		if (params != null) {
			for (String s : params) {
				tds.ajouterSymbole(new SymboleParam(s, idf, i));
				i++;
			}
			int nbParam = 0;
			for (Symbole sym : tds.getTableDesSymboles()) {
				if (sym.getGenre() == Genre.PARAM && sym.getScope() == idf) {
					nbParam++;
				}
			}
			((SymboleFonction) tds.getTableDesSymboles().get(tds.getTableDesSymboles().indexOf(symFun))).setNbParametre(nbParam);
		}
		for (Symbole sym : tdsTempVar.getTableDesSymboles()) {
			if (sym.getScope() == null) {
				sym.setScope(idf);
			}
			tds.ajouterSymbole(sym);
		}
		tdsTempVar.getTableDesSymboles().clear();
	}

	// Vérifie que les variables utilisées sont définies dans leurs scopes d'utilisation
	public void verifierVars(ArrayList<String> usedVar, String idf) throws Exception {
		for (String s : usedVar) {
			Symbole symboleCible = tds.findSymbole(s, idf);
			if (symboleCible != null) {
				if (symboleCible.getScope() != idf && symboleCible.getScope() != "global") {
					throw new Exception("Variable non définie dans le scope");
				}
			} else {
				throw new Exception("Variable non définie");
			}
		}
		usedVar.clear();
	}
	// Vérifie que les fonctions soient bien définie et qu'ils sont bien appelés avec le bon nombre de paramètres
	public void verifierFonction(ArrayList<String> usedFun, ArrayList<Integer> usedFunParam) throws Exception {
		int j = 0;
		for (String s : usedFun) {
			Symbole symboleCible = tds.findSymbole(s, "global");
			if (symboleCible != null) {
				if (symboleCible.getGenre() == Genre.FONCTION && symboleCible.getScope() != "global") {
					throw new Exception("Fonction non définie");
				}
				if (((SymboleFonction)symboleCible).getNbParametre() != usedFunParam.get(j)) {
					throw new Exception("Fonction appelé avec un nombre incohérent de paramètre");
				}
			} else {
				throw new Exception("Fonction non définie");
			}
			j++;
		}
		usedFun.clear();
		usedFunParam.clear();
	}
:}

/* Terminaux */
terminal			BRO, BRC, LPAREN, RPAREN;
terminal			COMMA, SEMI;
terminal String 	IDF;
terminal Integer	CONST;
terminal			VOID, MAIN, INT;
terminal			READ, WRITE;
terminal			PLUS, LESS, DIV, MUL;
terminal			LOWER, LOWERT, GREATER, GREATERT, ISEQUAL, ISDIF;
terminal			RETURN;
terminal			EQUAL;
terminal			WHILE, IF, ELSE;

/* Non terminaux */
non terminal definition_var_global;
non terminal definition_var_local;
non terminal ArrayList<String> vars;
non terminal Noeud liste, definition_fonction, definition_fonction_principale;
non terminal Bloc bloc;
non terminal Noeud instructions;
non terminal Noeud atome;
non terminal Noeud affectation;
non terminal Noeud condition, iteration;
non terminal Noeud boolean_expression;
non terminal Noeud expression;
non terminal Noeud ret;
non terminal Noeud appel;
non terminal Noeud ecrire;
non terminal ArrayList<Noeud> parametresAppel;
non terminal ArrayList<String> parametres;
non terminal Noeud facteur;
non terminal prog;

/* Axiome/Start */
start with prog;

/* Regles de production */
prog ::=	liste	{:	uasm.genererCode(); :}
	;

liste ::= 	definition_var_global SEMI liste
	|		definition_fonction:fct liste			{: resultat.ajouterUnFils(fct);	:}
	|		definition_fonction_principale:fct_p	{: resultat.ajouterUnFils(fct_p); :}
	;

definition_var_global ::=	INT IDF:idf vars:var EQUAL CONST:c	{:	addVarsToTds(idf, var, "global", c);	:}
	|						INT IDF:idf vars:var				{:	addVarsToTds(idf, var, "global", 0);	:}
	;

definition_var_local ::=	INT IDF:idf vars:var EQUAL CONST:c	{:	addVarsToTdsTemp(idf, var, c);			:}
	|						INT IDF:idf vars:var				{:	addVarsToTdsTemp(idf, var, 0);			:}
	;

vars ::=	COMMA IDF:idf vars:var	{:	if (var == null)
											var = new ArrayList<String>();
										var.add(idf);
										RESULT = var; :}
	|								{: RESULT = null; :}
	;

definition_fonction ::=	VOID IDF:idf LPAREN parametres:param RPAREN BRO bloc:bl BRC		{:	addAllToTds(idf, Type.VOID, param);
																						verifierVars(usedVar, idf);
																						verifierFonction(usedFun, usedFunParam);
																						RESULT = new Fonction(idf);
																						((Fonction) RESULT).ajouterUnFils(bl);	:} 
	|					INT IDF:idf LPAREN parametres:param RPAREN BRO bloc:bl BRC		{:	addAllToTds(idf, Type.INT, param);
																						verifierVars(usedVar, idf);
																						verifierFonction(usedFun, usedFunParam);
																						RESULT = new Fonction(idf);
																						((Fonction) RESULT).ajouterUnFils(bl);	:}
	;

definition_fonction_principale ::=	VOID MAIN LPAREN RPAREN BRO bloc:bl BRC				{:	addAllToTds("main", Type.VOID, null);
																						verifierVars(usedVar, "main");
																						verifierFonction(usedFun, usedFunParam);
																						RESULT = new Fonction("main");
																						((Fonction) RESULT).ajouterUnFils(bl);	:}
	;

bloc ::= instructions:ins bloc:b	{:	if (b == null) b = new Bloc();
										if (ins != null) b.ajouterUnFils(ins);
										RESULT = b;	:}
	|								{:	RESULT = new Bloc();	:}
	;

instructions ::=	condition:c 				{: RESULT = c; :}
	|				definition_var_local SEMI
	|				iteration:i					{: RESULT = i; :}
	|				appel:call SEMI				{: RESULT = call; :}
	|				ecrire:e SEMI				{: RESULT = e; :}
	|				ret:r SEMI					{: RESULT = r; :}
	|				affectation:af SEMI			{: RESULT = af; :}
	;

condition ::=	IF LPAREN boolean_expression:x RPAREN BRO bloc:y BRC ELSE BRO bloc:z BRC	{:	RESULT = new Si(compteurSi);
																								compteurSi++;
																								((Si) RESULT).setCondition(x);
																								((Si) RESULT).setBlocAlors(y);
																								((Si) RESULT).setBlocSinon(z);	:}
	|			IF LPAREN boolean_expression:x RPAREN BRO bloc:y BRC	{:	RESULT = new Si(compteurSi);
																			compteurSi++;
																			((Si) RESULT).setCondition(x);
																			((Si) RESULT).setBlocAlors(y);	:}
	;

iteration ::=	WHILE LPAREN boolean_expression:x RPAREN BRO bloc:y BRC	{:	RESULT = new TantQue(compteurTq);
																			compteurTq++;
																			((TantQue) RESULT).setCondition(x);
																			((TantQue) RESULT).setBlocAlors(y);	:}
	;

affectation ::=	IDF:idf EQUAL expression:e	{:	RESULT = new Affectation();
												((Affectation) RESULT).setFilsGauche(new Idf(idf));
												((Affectation) RESULT).setFilsDroit(e);
												usedVar.add(idf);	:}
	;

ret ::=	RETURN expression:x	{:	RESULT = new Retour("");
								((Retour) RESULT).setLeFils(x); :}
	;

expression ::=	expression:e PLUS facteur:f	{:	RESULT = new Plus();
												((Plus) RESULT).setFilsGauche(e);
												((Plus) RESULT).setFilsDroit(f);	:}
	|			expression:e LESS facteur:f {:	RESULT = new Moins();
												((Moins) RESULT).setFilsGauche(e);
												((Moins) RESULT).setFilsDroit(f);	:}
	|			facteur:f {:	RESULT = f;	:}
	;

facteur ::=		facteur:f MUL atome:a {:	RESULT = new Multiplication();
											((Multiplication) RESULT).setFilsGauche(f);
											((Multiplication) RESULT).setFilsDroit(a);	:}
	|			facteur:f DIV atome:a {:	RESULT = new Division();
											((Division) RESULT).setFilsGauche(f);
											((Division) RESULT).setFilsDroit(a);	:}
	|			atome:a {: RESULT = a; :}
	|			PLUS atome:a {:	RESULT = new Plus();
								((Plus) RESULT).setFilsDroit(a);
								((Plus) RESULT).setFilsGauche(new Const(0));	:}
	|			LESS atome:a {:	RESULT = new Moins();
								((Moins) RESULT).setFilsDroit(a);
								((Moins) RESULT).setFilsGauche(new Const(0));	:}
	;

appel ::=	IDF:i LPAREN parametresAppel:x RPAREN		{:	RESULT = new Appel(i);
															if (x != null) {
																RESULT.ajouterDesFils(x);
															}
															usedFun.add(i);
															usedFunParam.add(compteurParamAppel);
															compteurParamAppel = 0;
														 :}
	;

parametresAppel ::= expression:e COMMA parametresAppel:pA 	{: 	if (pA == null) {
																	pA = new ArrayList<Noeud>();
																}
																pA.add(0, e);
																compteurParamAppel++;
																RESULT = pA; :}
		|			expression:e 							{:	ArrayList<Noeud> pA = new ArrayList<Noeud>(); 
																pA.add(0, e);
																compteurParamAppel++;
																RESULT = pA; :}
		|													{:	RESULT = null;	:}
		;

parametres ::=	INT IDF:idf COMMA parametres:param	{:	if (param == null)
															param = new ArrayList<String>();
														param.add(0, idf);
														RESULT = param;	:}
		| 		INT IDF:idf	{:	ArrayList<String> param = new ArrayList<String>();
								param.add(0, idf);
								RESULT = param;	:}
		|	{: RESULT = null; :}
		;

ecrire ::=	WRITE LPAREN atome:x RPAREN {: 	RESULT = new Ecrire();
											((Ecrire) RESULT).setLeFils(x); :}
		;

boolean_expression ::=	expression:x LOWER boolean_expression:be	{:	RESULT = new Inferieur();
																		((Inferieur) RESULT).setFilsGauche(x);
																		((Inferieur) RESULT).setFilsDroit(be);	:}
		|				expression:x LOWERT boolean_expression:be	{:	RESULT = new InferieurEgal();
																		((InferieurEgal) RESULT).setFilsGauche(x);
																		((InferieurEgal) RESULT).setFilsDroit(be);	:}
		|				expression:x GREATER boolean_expression:be	{:	RESULT = new Superieur();
																		((Superieur) RESULT).setFilsGauche(x);
																		((Superieur) RESULT).setFilsDroit(be);	:}
		|				expression:x GREATERT boolean_expression:be	{:	RESULT = new SuperieurEgal();
																		((SuperieurEgal) RESULT).setFilsGauche(x);
																		((SuperieurEgal) RESULT).setFilsDroit(be);	:}
		|				expression:x ISEQUAL boolean_expression:be	{:	RESULT = new Egal();
																		((Egal) RESULT).setFilsGauche(x);
																		((Egal) RESULT).setFilsDroit(be);	:}
		|				expression:x ISDIF boolean_expression:be	{:	RESULT = new Different();
																		((Different) RESULT).setFilsGauche(x);
																		((Different) RESULT).setFilsDroit(be);	:}
		|				expression:x								{:	RESULT = x;	:}
		;

atome ::= 	CONST:c						{:	RESULT = new Const(c);	:}
	|		IDF:idf						{:	RESULT = new Idf(idf);
											usedVar.add(idf);		:}
	|		READ LPAREN RPAREN			{:	RESULT = new Lire();	:}
	|		appel:a						{:	RESULT = a;	:}
	|		LPAREN expression:x RPAREN	{:	RESULT = x;	:}
	;