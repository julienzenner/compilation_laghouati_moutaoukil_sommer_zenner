package generated.fr.ul.miashs.compilation;
import java_cup.runtime.Symbol;

%%

/* options */
%line
%public
%cupsym Sym
%cup

/* macros */
SEP     =   [ \t]
RC      =   "\n"|"\r\n"
CONST	=	[0-9]+
IDF     =   [a-zA-Z][a-zA-Z0-9]*
COMMENT	=	(\/\/.*\n)|(\/\*(([^*])|(\*[^\/]))*\*\/)

%%

/* regles */
"void"			{return new Symbol(Sym.VOID);}
"main"			{return new Symbol(Sym.MAIN);}
"int"			{return new Symbol(Sym.INT);}
"("				{return new Symbol(Sym.LPAREN);}
")"				{return new Symbol(Sym.RPAREN);}
"{"				{return new Symbol(Sym.BRO);} 
"}"				{return new Symbol(Sym.BRC);} 
","				{return new Symbol(Sym.COMMA);} 
";"				{return new Symbol(Sym.SEMI);} 
"="				{return new Symbol(Sym.EQUAL);} 
"+"				{return new Symbol(Sym.PLUS);} 
"-"				{return new Symbol(Sym.LESS);} 
"*"				{return new Symbol(Sym.MUL);} 
"/"				{return new Symbol(Sym.DIV);}
"return"		{return new Symbol(Sym.RETURN);} 
"if"			{return new Symbol(Sym.IF);} 
"else"			{return new Symbol(Sym.ELSE);} 
"while"			{return new Symbol(Sym.WHILE);} 
"read"			{return new Symbol(Sym.READ);}
"write"			{return new Symbol(Sym.WRITE);}
"<"				{return new Symbol(Sym.LOWER);}
"<="			{return new Symbol(Sym.LOWERT);}
">"				{return new Symbol(Sym.GREATER);} 
">="			{return new Symbol(Sym.GREATERT);}
"=="			{return new Symbol(Sym.ISEQUAL);}
"!="			{return new Symbol(Sym.ISDIF);}
{SEP}      		{;} 
{RC}			{;}
{CONST}			{return new Symbol(Sym.CONST, new Integer(yytext()));}
{IDF} 			{return new Symbol(Sym.IDF, yytext());}
{COMMENT} 		{;}
.				{return null;}