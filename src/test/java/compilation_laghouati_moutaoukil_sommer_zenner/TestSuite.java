package compilation_laghouati_moutaoukil_sommer_zenner;

import java.io.FileReader;
import java.io.Reader;

import org.junit.Test;

import generated.fr.ul.miashs.compilation.ParserCup;
import generated.fr.ul.miashs.compilation.Yylex;

public class TestSuite {

	@Test
	public void test1() throws Exception {
		String filename = "samples/test-suite/e" + 1 + ".data";
		Reader reader = new FileReader(filename);
		Yylex scanner = new Yylex(reader);
		@SuppressWarnings("deprecation")
		ParserCup parser = new ParserCup(scanner);
		parser.parse();
	}

	@Test
	public void test2() throws Exception {
		String filename = "samples/test-suite/e" + 2 + ".data";
		Reader reader = new FileReader(filename);
		Yylex scanner = new Yylex(reader);
		@SuppressWarnings("deprecation")
		ParserCup parser = new ParserCup(scanner);
		parser.parse();
	}

	@Test
	public void test3() throws Exception {
		String filename = "samples/test-suite/e" + 3 + ".data";
		Reader reader = new FileReader(filename);
		Yylex scanner = new Yylex(reader);
		@SuppressWarnings("deprecation")
		ParserCup parser = new ParserCup(scanner);
		parser.parse();
	}

	@Test
	public void test4() throws Exception {
		String filename = "samples/test-suite/e" + 4 + ".data";
		Reader reader = new FileReader(filename);
		Yylex scanner = new Yylex(reader);
		@SuppressWarnings("deprecation")
		ParserCup parser = new ParserCup(scanner);
		parser.parse();
	}

	@Test
	public void test5() throws Exception {
		String filename = "samples/test-suite/e" + 5 + ".data";
		Reader reader = new FileReader(filename);
		Yylex scanner = new Yylex(reader);
		@SuppressWarnings("deprecation")
		ParserCup parser = new ParserCup(scanner);
		parser.parse();
	}

	@Test
	public void test6() throws Exception {
		String filename = "samples/test-suite/e" + 6 + ".data";
		Reader reader = new FileReader(filename);
		Yylex scanner = new Yylex(reader);
		@SuppressWarnings("deprecation")
		ParserCup parser = new ParserCup(scanner);
		parser.parse();
	}

	@Test
	public void test7() throws Exception {
		String filename = "samples/test-suite/e" + 7 + ".data";
		Reader reader = new FileReader(filename);
		Yylex scanner = new Yylex(reader);
		@SuppressWarnings("deprecation")
		ParserCup parser = new ParserCup(scanner);
		parser.parse();
	}

	@Test
	public void test8() throws Exception {
		String filename = "samples/test-suite/e" + 8 + ".data";
		Reader reader = new FileReader(filename);
		Yylex scanner = new Yylex(reader);
		@SuppressWarnings("deprecation")
		ParserCup parser = new ParserCup(scanner);
		parser.parse();
	}
	/*
	 * Test qui ne passe logiquement pas
	 */
//	@Test
//	public void test9() throws Exception {
//		String filename = "samples/test-suite/e" + 9 + ".data";
//		Reader reader = new FileReader(filename);
//		Yylex scanner = new Yylex(reader);
//		@SuppressWarnings("deprecation")
//		ParserCup parser = new ParserCup(scanner);
//		parser.parse();
//	}
//
//	@Test
//	public void test10() throws Exception {
//		String filename = "samples/test-suite/e" + 10 + ".data";
//		Reader reader = new FileReader(filename);
//		Yylex scanner = new Yylex(reader);
//		@SuppressWarnings("deprecation")
//		ParserCup parser = new ParserCup(scanner);
//		parser.parse();
//	}
//
//	@Test
//	public void test11() throws Exception {
//		String filename = "samples/test-suite/e" + 11 + ".data";
//		Reader reader = new FileReader(filename);
//		Yylex scanner = new Yylex(reader);
//		@SuppressWarnings("deprecation")
//		ParserCup parser = new ParserCup(scanner);
//		parser.parse();
//	}
//
//	@Test
//	public void test12() throws Exception {
//		String filename = "samples/test-suite/e" + 12 + ".data";
//		Reader reader = new FileReader(filename);
//		Yylex scanner = new Yylex(reader);
//		@SuppressWarnings("deprecation")
//		ParserCup parser = new ParserCup(scanner);
//		parser.parse();
//	}
//
//	@Test
//	public void test13() throws Exception {
//		String filename = "samples/test-suite/e" + 13 + ".data";
//		Reader reader = new FileReader(filename);
//		Yylex scanner = new Yylex(reader);
//		@SuppressWarnings("deprecation")
//		ParserCup parser = new ParserCup(scanner);
//		parser.parse();
//	}
}
